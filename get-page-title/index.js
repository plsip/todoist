module.exports = async function (browser, event) {
  console.log("event", event)
  
  if (!event.inputParameters.testTargetUrl) {
    throw 'Invalid event data!';
  }

  await browser.get(event.inputParameters.testTargetUrl);
  const title = await browser.getTitle();

  return { pageTitle: title };
};

